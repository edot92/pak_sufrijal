module.exports = {
  mqtt: require('mqtt'),
  clientMqtt: null,
  init () {
    let _this = this
    return new Promise((resolve, reject) => {
      var client = _this.mqtt.connect('mqtt://test.mosquitto.org:1883')

      client.on('connect', function () {
        _this.clientMqtt = client
        resolve(client)
      })
    })
  }
}
