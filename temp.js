require('dotenv').config()
let csvToJson = require('convert-csv-to-json')
const { S7Client } = require('s7client')
let mqttjs = require('./mqtt_client')
let base_topik = 'test/base_topik'
let topik = {
  read: base_topik + 'read',
  write: base_topik + 'write',
  qty_widget: base_topik + 'qty_widget',
  online: base_topik + 'online',
  offline: base_topik + 'offline',
  error: base_topik + 'error'
}

async function loop () {
//   setInterval(() => {
  // mqttjs.clientMqtt.publish(topik.read, 'tes123')
//   }, 2000)
}
async function init () {
  if (mqttjs.clientMqtt === null) {
    await mqttjs.init()
  }

  mqttjs.clientMqtt.on(topik.write, function (params) {

  })
  let json = csvToJson.getJsonFromCsv('./register.csv')
  // console.log(json.length)
  for (let i1 = 0; i1 < json.length; i1++) {
    let el1 = json[i1]
    for (let i2 = 0; i2 < el1.length; i2++) {
      let el2 = el1[i2]
      el2 = String(el2).replace('\r', '')
    }
  }
  // PLC Connection Settings
  const plcSettings = {
    name: 'PLC_KLIENT',
    host: process.env.host_plc,
    port: parseInt(process.env.port_plc),
    rack: parseInt(process.env.rack_plc),
    slot: parseInt(process.env.slot_plc)
  }
  //   console.log(plcSettings)
  // DBA to read
  let client = new S7Client(plcSettings)
  let isReadyQuee = false
  setInterval(() => {
    if (isReadyQuee === false) {
      console.log('ready proses plc')
      mqttjs.clientMqtt.publish(topik.qty_widget, json.length.toString())

      isReadyQuee = true
      client.on('error', function (errMsg) {
        console.log(errMsg)
        setTimeout(() => {
          isReadyQuee = false
        }, 1000)
      });
      (async function () {
        try {
          const cpuInfo = await client.connect()
        } catch (error) {
          console.error(error)
          mqttjs.clientMqtt.publish(topik.error, String(error))
          return
        }
        //   console.log(cpuInfo)
        let tempResp = []
        for (let i1 = 0; i1 < json.length; i1++) {
          let el1 = json[i1]
          let el2 = el1
          let dbNr = el2.no_db
          //   console.log(el2)
          let dbVars =
            {
              dbnr: parseInt(dbNr),
              area: 'db',
              type: el2.type_register,
              start: parseInt(el2.no_register)
            }
          //   const res = await client.readDB(dbNr, dbVars)

          try {
            // const res = await client.readVar({
            //   type: 'REAL',
            //   start: 1,
            //   area: 'db',
            //   dbnr: 1
            // })
            // let temp1 = {
            //   type: 'REAL',
            //   start: 1,
            //   area: 'db',
            //   dbnr: 1
            // }
            // console.log(dbVars)
            // console.log(temp1)
            const res = await client.readVar(dbVars)
            // console.log(res)
            mqttjs.clientMqtt.publish(topik.read, JSON.stringify(res))
            tempResp.push(res)
          } catch (error) {
            console.error(error)
            mqttjs.clientMqtt.publish(topik.error, String(error))
          }
          mqttjs.clientMqtt.publish(topik.read, Buffer.from(tempResp))
          //   console.log(json[i1])
        }
        try {
          client.disconnect()
        } catch (error) {
          mqttjs.clientMqtt.publish(topik.error, String(error))
        }
        setTimeout(() => {
          isReadyQuee = false
        }, 1000)
      })()
    }
  }, 2000)
}

require('./web.js')

init()
loop()
